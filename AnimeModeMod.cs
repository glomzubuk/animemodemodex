﻿using System;
using System.Collections.Generic;
using System.Reflection;
using BepInEx;
using BepInEx.Configuration;
using HarmonyLib;
using UnityEngine;
using LLHandlers;
using GameplayEntities;
using LLBML;
using LLBML.Players;
using LLBML.Math;
using LLBML.States;
using LLBML.Utils;

namespace AnimeModeMod
{
    [BepInPlugin(PluginInfos.PLUGIN_ID, PluginInfos.PLUGIN_NAME, PluginInfos.PLUGIN_VERSION)]
    [BepInDependency(LLBML.PluginInfos.PLUGIN_ID, BepInDependency.DependencyFlags.HardDependency)]
    [BepInDependency("no.mrgentle.plugins.llb.modmenu", BepInDependency.DependencyFlags.SoftDependency)]
    [BepInProcess("LLBlaze.exe")]
    public class AnimeModeMod : BaseUnityPlugin
    {

        private static readonly MethodInfo getSpareEffectInfo = AccessTools.Method(typeof(EffectHandler), "GetSpareEffect");
        public delegate EffectEntity getSpareEffectDelegate();
        private getSpareEffectDelegate getSpareEffect;

        private ConfigEntry<KeyCode> toggleConsoleKey;
        private ConfigEntry<KeyCode> enableAuraKey;
        private ConfigEntry<KeyCode> enablePhantomKey;
        private ConfigEntry<KeyCode> toggleDebugKey;
        private ConfigEntry<int> auraSpeedTrigger;
        private ConfigEntry<int> phantomSpeedTrigger;
        private ConfigEntry<int> blazeEffectHeight;
        private ConfigEntry<bool> enableAuraOffline;
        private ConfigEntry<bool> enableAuraOnline;

        private bool enableDebug = false;

        // Awake is called once when both the game and the plug-in are loaded
        void Awake()
        {
            Logger.LogInfo("Hello, world!");
            this.InitConfig();
        }

        void Start()
        {
            ModDependenciesUtils.RegisterToModMenu(this.Info, new List<string> { "This is an example text to see if the text window is working" });
        }

        public BallEntity currentBall = null;
        bool playersInAnime = false;
        bool playersInPhantom = false;


        private void Update()
        {
            if (Input.GetKeyDown(toggleDebugKey.Value))
            {
                enableDebug = !enableDebug;
            }
            GameMode currentGameMode = StateApi.CurrentGameMode;
            bool allowedGameMode = (currentGameMode == GameMode._1v1 || currentGameMode == GameMode.TRAINING || currentGameMode == GameMode.FREE_FOR_ALL || currentGameMode == GameMode.COMPETITIVE);

            if (GameStates.IsInMatch() && allowedGameMode)
            {
                if (Input.GetKeyDown(enableAuraKey.Value))
                {
                    //DebugSettings.instance.doEclipseOnTaunt = true;
                    Player.ForAllActivelyInMatch(TurnPlayerIntoAnime);
                    //playersInAnime = true;
                }
                if (Input.GetKeyDown(enablePhantomKey.Value))
                {
                    Player.ForAllActivelyInMatch(TurnPlayerIntoPhantom);
                    //playersInPhantom = true;
                }
                currentBall = BallApi.GetBall();
                if (currentBall != null)
                {
                    if ((NetworkApi.IsOnline && enableAuraOnline.Value) || (!NetworkApi.IsOnline && enableAuraOffline.Value))
                    {
                        UpdateAnimeState(currentBall);
                    }
                }
            }
            else
            {
                currentBall = null;
                getSpareEffect = null;
                playersInAnime = false;
            }
        }

        private void UpdateAnimeState(BallEntity ball)
        {

            /*if (Input.GetKey(KeyCode.N))
            {
                ball.SetVisualSprite("main2D", Texture2D.whiteTexture, true, false, BallEntity.GetFrameSize(), JKMAAHELEMF.DBOMOJGKIFI, 0f, false, 1f, Layer.GAMEPLAY, default(Color32));
            }*/

            bool ballRespawing = ball.ballData.ballState == BallState.APPEAR || ball.ballData.ballState == BallState.APPEAR_HIGH;
            if (ballRespawing && (playersInAnime || playersInPhantom))
            {
                Player.ForAllInMatch(CancelPlayerAnimations);
                playersInAnime = false;
                playersInPhantom = false;
            }
            else if (!playersInAnime && (Floatf)ball.GetPixelFlySpeed(true) >= auraSpeedTrigger.Value)
            {
                Player.ForAllActivelyInMatch(TurnPlayerIntoAnime);
                playersInPhantom = false;
                playersInAnime = true;
            }
            else if (!playersInPhantom && !playersInAnime && (Floatf)ball.GetPixelFlySpeed(true) >= phantomSpeedTrigger.Value)
            {
                Player.ForAllActivelyInMatch(TurnPlayerIntoPhantom);
                playersInPhantom = true;
            }
            else
            {
                if (playersInPhantom)
                {
                    Player.ForAllActivelyInMatch(UpdatePlayerPhantom);
                }
                if (playersInAnime)
                {
                    Player.ForAllActivelyInMatch(UpdatePlayerAnime);
                }
            }
        }

        private void TurnPlayerIntoPhantom(PlayerEntity playerEntity)
        {
            playerEntity.PlayAnim("phantom", "phantomVisual");
        }

        private void TurnPlayerIntoAnime(PlayerEntity playerEntity)
        {
            Logger.LogDebug("Turning players anime.");

            playerEntity.ChangeIntoSilhouette(true);
            playerEntity.PlayAnim("off", "phantomVisual");
            playerEntity.PlayAnim("aura", "auraVisual");
            CreateGetUpBlazeEffectOnPos(playerEntity.GetPosition(), playerEntity.GetTeam());
        }

        private void UpdatePlayerPhantom(PlayerEntity playerEntity)
        {
            if (!playerEntity.hasAnimation("phantom"))
            {
                Logger.LogDebug("Updating phantom anim.");
                playerEntity.PlayAnim("phantom", "phantomVisual");
            }
        }

        private void UpdatePlayerAnime(PlayerEntity playerEntity)
        {
            playerEntity.ChangeIntoSilhouette(true);
            if (!playerEntity.hasAnimation("aura"))
            {
                Logger.LogDebug("Updating aura anim.");
                playerEntity.PlayAnim("aura", "auraVisual");

            }
        }

        private void CancelPlayerAnimations(PlayerEntity playerEntity)
        {
            playerEntity.ChangeIntoSilhouette(false);
            playerEntity.PlayAnim("off", "phantomVisual");
            playerEntity.PlayAnim("off", "auraVisual");
        }


        public void CreateGetUpBlazeEffectOnPos(Vector2f pos, BGHNEHPFHGC team)
        {
            if (getSpareEffect == null && EffectHandler.instance != null)
            {
                getSpareEffect = AccessTools.MethodDelegate<getSpareEffectDelegate>(getSpareEffectInfo, EffectHandler.instance);
                if (getSpareEffect == null) return;
            }
            if (EffectHandler.noEffects)
            {
                return;
            }
            pos.y = pos.y + blazeEffectHeight.Value * (Floatf)World.FPIXEL_SIZE;
            EffectEntity spareEffect = this.getSpareEffect.Invoke();
            EffectData effectData = new EffectData(1)
            {
                active = true,
                offsetPixels = new Vector2i(0, -20),
                frameSizePixels = new Vector2i(400, 250),
                animSpeed = (Floatf)24.0m,
                scale = 1.2f,
                endAtAnimEnd = true,
                layer = Layer.INFRONT_GAMEPLAY,
                graphicName = "getDrawIn" + JPLELOFJOOH.OODCFCKNLOD(team)
            };
            effectData.SetPositionData(pos);
            spareEffect.ApplyEffectData(effectData, false);
        }

        private void OnGUI()
        {
            GUI.contentColor = Color.red;
            if (enableDebug)
            {
                GUI.Label(new Rect(10, 10, 200, 25), "Debug Info: On");

                GUI.Label(new Rect(10, 35, 200, 25), "Is the game Online : " + (NetworkApi.IsOnline? "Yes" : "No"));
                GUI.Label(new Rect(10, 60, 500, 25), "The Big Question: " + (GameStates.IsInMatch() ? "Yes" : "No"));
                GUI.Label(new Rect(210, 35, 200, 25), "What's the debug key : " + toggleDebugKey.Value.ToString());
                GUI.Label(new Rect(210, 60, 200, 25), "What's the aura key : " + enableAuraKey.Value.ToString());

                GUI.Label(new Rect(10, 85, 500, 25), "Current Game mode: " + StateApi.CurrentGameMode.ToString());
                GUI.Label(new Rect(10, 110, 500, 25), "Current Game State: " + GameStates.GetCurrent().ToString());
                if (GameStates.IsInMatch() && currentBall != null)
                {
                    GUI.Label(new Rect(10, 135, 250, 25), "Ball State: " + currentBall.ballData.ballState.ToString());
                    GUI.Label(new Rect(260, 135, 500, 25), "Ball HitstunState: " + currentBall.ballData.hitstunState.ToString());
                    GUI.Label(new Rect(10, 160, 500, 25), "Ball FlySpeed: " + currentBall.GetFlySpeed(true));
                    GUI.Label(new Rect(10, 185, 500, 25), "Ball PixelFlySpeed: " + currentBall.GetPixelFlySpeed(true));
                }
            }
        }

        private void InitConfig()
        {
            toggleConsoleKey = Config.Bind(
                "General.Toggles",
                "ConsoleKey",
                KeyCode.BackQuote,
                "Key to spawn the console");
            enableAuraKey = Config.Bind(
                "General.Toggles",
                "AuraKey",
                KeyCode.B,
                "Key to enable Aura on a whim");
            enablePhantomKey = Config.Bind(
                "General.Toggles",
                "PhantomKey",
                KeyCode.N,
                "Key to enable Phantom on a whim");
            toggleDebugKey = Config.Bind(
                "General.Toggles",
                "DebugKey",
                KeyCode.K,
                "Key that toggles the debug menu");
            auraSpeedTrigger = Config.Bind(
                "General.Tuning",
                "AuraSpeedTrigger",
                256,
                "Trigger for enabling the aura effect");
            phantomSpeedTrigger = Config.Bind(
                "General.Tuning",
                "PhantomSpeedTrigger",
                128,
                "Trigger for enabling the phantom effect");
            blazeEffectHeight = Config.Bind(
                "General.Tuning",
                "BlazeEffectHeight",
                28,
                new ConfigDescription(
                    "Height at which to spawn the blaze effect on the aura trigger",
                    new AcceptableValueRange<int>(0, 100)
                )
            );
            enableAuraOffline = Config.Bind(
                "General.Toggles",
                "EnableAuraOffline",
                true,
                "Should the effects be enabled offline"
            );
            enableAuraOnline = Config.Bind(
                "General.Toggles",
                "EnableAuraOnline",
                true,
                "Should the effects be enabled online"
            );
        }
    }
}
